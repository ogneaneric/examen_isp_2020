package ognean.eric.examen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiect2 {
    private JPanel subiect2;
    private JButton reverseItButton;
    private JTextField textField1;
    private JTextArea textArea1;


    public JPanel getSubiect1() {
        return subiect2;
    }

    public void setSubiect1(JPanel subiect1) {
        this.subiect2 = subiect1;
    }

    public JButton getReverseItButton() {
        return reverseItButton;
    }

    public void setReverseItButton(JButton reverseItButton) {
        this.reverseItButton = reverseItButton;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }


        public String reverseString(String str){
            char ch[]=str.toCharArray();
            String ogl="";
            for(int i=ch.length-1;i>=0;i--){
                ogl+=ch[i];
            }
            return ogl;
        }


    public Subiect2() {
        reverseItButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            String text = new String();
            text=textField1.getText();
            textArea1.setText(reverseString(text));
                System.out.println(reverseString(text));

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Subiect1");
        frame.setContentPane(new Subiect2().subiect2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
