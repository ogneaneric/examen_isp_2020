package ognean.eric.examen;

public class Subiect1 {

    class K {

    }

    class L extends K {
        private int a;

        public void b() {
        }

        public void i() {
        }
    }

    class X {
        void met(L l) {
        }
    }


    class M {
        private B unnume;

        public void metoda() {
            unnume = new B();
        }
    }

    class A {
        M nume;
    }

    class B {

    }


}
